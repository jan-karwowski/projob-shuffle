#include <algorithm>
#include <cassert>
#include <iostream>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

using std::vector;

int distance(const vector<int> &v1, const vector<int> &v2);
vector<std::pair<std::string, vector<int>>> parse_csv(std::istream &stream);
void write_csv(std::ostream &stream,
               const vector<std::pair<std::string, vector<int>>> &vec);
vector<std::pair<std::string, vector<int>>>
append_variants(const vector<std::pair<std::string, vector<int>>> &assignments,
                int appended_variants_count);

int main(int argc, char **argv) {
  assert(argc >= 1);
  int count = std::stoi(argv[1]);
  auto rec = parse_csv(std::cin);
  auto n = append_variants(rec, count);
  write_csv(std::cout, n);
  return 0;
}

vector<std::pair<std::string, vector<int>>> parse_csv(std::istream &stream) {
  vector<std::pair<std::string, vector<int>>> output;
  std::string line;
  while (std::getline(stream, line)) {
    auto it = std::find(line.begin(), line.end(), ',');
    std::string id(line.begin(), it);
    vector<int> assignments;
    while (it != line.end()) {
      it++;
      auto delim = std::find(it, line.end(), ',');
      std::string field(it, delim);
      assignments.push_back(std::stoi(field));
      it = delim;
    }
    output.push_back(std::make_pair(id, assignments));
  }
  return output;
}

void write_csv(std::ostream &stream,
               const vector<std::pair<std::string, vector<int>>> &vec) {
  for (auto row : vec) {
    stream << row.first;
    for (auto field : row.second) {
      stream << ',' << field;
    }
    stream << std::endl;
  }
}

vector<std::pair<std::string, vector<int>>>
append_variants(const vector<std::pair<std::string, vector<int>>> &assignments,
                int appended_variants_count) {
  vector<std::pair<int, std::pair<std::string, vector<int>>>> with_distances;
  std::transform(
      assignments.begin(), assignments.end(),
      std::back_inserter(with_distances),
      [&assignments](const std::pair<std::string, vector<int>> &assignment) {
        int dist = std::transform_reduce(
            assignments.begin(), assignments.end(),
            (int)assignments.begin()->second.size(),
            [](int a, int b) { return std::min(a, b); },
            [&assignment](const std::pair<std::string, vector<int>> &val) {
              if (val.first == assignment.first) {
                return (int)assignment.second.size();
              } else {
                return distance(assignment.second, val.second);
              }
            });
        return make_pair(dist, assignment);
      });

  std::sort(with_distances.begin(), with_distances.end(),
            [](auto &v1, auto &v2) { return v1.first < v1.first; });

  vector<std::pair<std::string, vector<int>>> output;

  for (const auto &assignment : with_distances) {
    auto new_assignment = assignment.second.second;
    new_assignment.push_back(0);
    int max = 0;
    int maxval = std::transform_reduce(
        output.begin(), output.end(), new_assignment.size(), std::plus<>(),
        [&new_assignment](const std::pair<std::string, vector<int>> &asg) {
          return distance(new_assignment, asg.second);
        });

    for (int i = 1; i < appended_variants_count; i++) {
      (*new_assignment.rbegin()) = i;
      int dist = std::transform_reduce(
          output.begin(), output.end(), new_assignment.size(), std::plus<>(),
          [&new_assignment](const std::pair<std::string, vector<int>> &asg) {
            return distance(new_assignment, asg.second);
          });
      if (dist > maxval) {
        maxval = dist;
        max = i;
      }
    }
    (*new_assignment.rbegin()) = max;
    output.push_back(std::make_pair(assignment.second.first, new_assignment));
  }

  return output;
}

int distance(const vector<int> &v1, const vector<int> &v2) {
  assert(v1.size() == v2.size());

  return std::transform_reduce(
      v1.begin(), v1.end(), v2.begin(), 0, std::plus<>(),
      [](int el1, int el2) { return el1 == el2 ? 0 : 1; });
}
